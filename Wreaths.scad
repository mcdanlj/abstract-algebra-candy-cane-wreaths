// Candy cane shaft
D=8;
// Inside radius of hook
R=15;
// Length of straight part outside hook
L=50;

$fn=180;
z_offset=D/10;
brim_h=0.2;

// Choose one to model and print
D4();
//C4();

module cane(D=D, R=R, L=L, r=1, brim_h=brim_h, brim_r=D) {
    rotation = r*atan((2*R)/(L-R/2));
    translate([0, 2*D*sin(rotation), 0])
    rotate([0, 0, -rotation])
    translate([-(R-D/2), L, D/2]) {
        rotate_extrude(angle=180) 
            translate([R, 0, 0])
            circle(d=D);
        translate([R, 0, 0]) rotate([90, 0, 0]) cylinder(h=L, d=D);
        translate([-R, 0, 0]) rotate([90, 0, 0]) cylinder(h=R/2, d=D);
        translate([R, -L, -D/2]) cylinder(h=brim_h, r=brim_r);
    }
}
module heart(brim_h=brim_h) {
    join = D/8;
    translate([-join, -join, 0]) cane();
    mirror([1, 0, 0]) translate([-join, -join, 0]) cane();
}
module bow(brim_h=brim_h) {
    heart(brim_h=brim_h);
    mirror([0, 1, 0]) heart(brim_h=brim_h);
}
module D4(z_offset=z_offset, brim_h=brim_h) {
    difference() {
        translate([0, 0, -z_offset]) {
            bow(brim_h=brim_h);
            rotate([0, 0, 90]) bow(brim_h=brim_h);
        }
        translate([0, 0, -z_offset]) cylinder(h=z_offset*2, r=2*L, center=true);
    }
    // brim around the inside
    difference() {
        cylinder(d=R*2, h=brim_h);
        cylinder(d=R, h=4*brim_h, center=true);
    }
}
module ccane(brim_h=brim_h) {
    // Tuned to join print
    translate([R-D/2, -(L+D/2+R*1.45), 0]) cane(r=0, brim_h=brim_h);
}
module C4(z_offset=z_offset, brim_h=brim_h) {
    difference() {
        translate([0, 0, -z_offset]) {
            ccane(brim_h=brim_h+z_offset);
            rotate([0, 0, 90]) ccane(brim_h=brim_h+z_offset);
            rotate([0, 0, 180]) ccane(brim_h=brim_h+z_offset);
            rotate([0, 0, 270]) ccane(brim_h=brim_h+z_offset);
        }
        translate([0, 0, -z_offset]) cylinder(h=z_offset*2, r=2*L, center=true);
    }
}

// Copyright © Michael K Johnson
// CC BY 4.0
