Candy Cane Abstract Algebra
===========================

These designs are intended as educational aids
for teaching about the cyclic and dihedral [symmetry
groups](https://en.wikipedia.org/wiki/Symmetry_group), supporting
an activity of glueing candy canes together into wreaths that
show the C4 [cyclic](https://en.wikipedia.org/wiki/Cyclic_group)
and D4 [dihedral](https://en.wikipedia.org/wiki/Dihedral_group)
symmetry groups, to gain intuition of reflective and rotational
symmetry.

These models are more durable than actual candy canes.  They
are designed to be durable rather than an exact model, but
have proven effective as teaching aids.

To render the models, choose `C4();` or `D4();` at the top of
the file.

Copyright © Michael K Johnson
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
